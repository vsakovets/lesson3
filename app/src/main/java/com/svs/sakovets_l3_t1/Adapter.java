package com.svs.sakovets_l3_t1;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    ArrayList<History> history;
    Context context;

    public Adapter(List<History> history, Context context) {
        this.history = (ArrayList<History>) history;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final History item = history.get(position);
        holder.content.setText(item.getContent());
        holder.text.setText(item.getText());
        holder.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,HistoryFull.class);

                //passing data to HistoryFull
                intent.putExtra("content", history.get(position).getContent() );
                intent.putExtra("text", history.get(position).getText() );
                context.startActivity(intent);
            }
        });
           }

    @Override
    public int getItemCount() {
        return history.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView content;
        TextView text;
        //  RatingBar ratingBar;

        public ViewHolder(View view) {
            super(view);
            content = view.findViewById(R.id.content_tv);
            text = view.findViewById(R.id.text_tv);

            //  ratingBar = view.findViewById(R.id.ratingBar);
        }
    }

}
