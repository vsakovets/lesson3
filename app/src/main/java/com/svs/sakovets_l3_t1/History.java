package com.svs.sakovets_l3_t1;

import android.widget.RatingBar;

public class History {
    String content;
    String text;
   // RatingBar ratingBar;

    public History(String content, String text/*, RatingBar ratingBar*/) {
        this.content = content;
        this.text = text;
      //  this.ratingBar = ratingBar;
    }

    public String getContent() {
        return content;
    }

    public String getText() {
        return text;
    }
}
