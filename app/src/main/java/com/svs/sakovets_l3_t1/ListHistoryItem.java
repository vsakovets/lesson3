package com.svs.sakovets_l3_t1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.RatingBar;

import java.util.ArrayList;
import java.util.List;

public class ListHistoryItem extends AppCompatActivity {
    RecyclerView recyclerView;
    Adapter adapter;
    List<History> listItems;
    String text = "True history of the man";
    String text2 = "It was an early Sunday morning, Michael woke up at 6 in thee morning? made coffe and went to the balcony to contemlate the dawn...";
  //  RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_history);

        recyclerView = findViewById(R.id.recyclerView);
       /* recyclerView.setHasFixedSize(true);*/
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();
        listItems.add(new History(text, text2));
        listItems.add(new History(text, text2));
        listItems.add(new History(text, text2));
        listItems.add(new History(text, text2));
        listItems.add(new History(text, text2));
        listItems.add(new History(text, text2));
        listItems.add(new History(text, text2));

        adapter=new Adapter(listItems,this);
recyclerView.setAdapter(adapter);

    }
}
