package com.svs.sakovets_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class HistoryFull extends AppCompatActivity {

    TextView tvContent;
    TextView tvText;
    Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_full);

        tvContent = findViewById(R.id.content_tv_hf);
        tvText = findViewById(R.id.text_tv_hf);
        btnAdd=findViewById(R.id.add_to_favourites);

        Intent intent = getIntent();
        String content = intent.getExtras().getString("content");
        String text = intent.getExtras().getString("text");

        tvContent.setText(content);
        tvText.setText(text);

btnAdd.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Toast toast = Toast.makeText(HistoryFull.this, "History was added to favourites", Toast.LENGTH_SHORT);
        toast.show();
    }
});


    }
}
