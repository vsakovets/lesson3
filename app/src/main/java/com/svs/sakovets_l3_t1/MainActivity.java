package com.svs.sakovets_l3_t1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnSignIn;
    Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSignIn = findViewById(R.id.btn_sign_in);
        btnSignUp = findViewById(R.id.btn_sign_up);
        btnSignIn.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
          }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.btn_sign_in:
                intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_sign_up:
                intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                break;
        }

    }
}